/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ifc.edu.br.mv7hib.model;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import java.util.ArrayList;

public class TestarCarro {

    public static void main(String[] args) {
        
        Carro p = new Carro("Audi A3", "Audi", "BNU-TESTE123");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("meuPU");
        EntityManager em = emf.createEntityManager();
        
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.persist(p);
        tx.commit();

        ArrayList<Carro> carros = (ArrayList<Carro>) em.createQuery("from Carro", Carro.class).getResultList();
        Carro a = carros.get(0);
        
        System.out.println(a);
        
        em.close();
    }
}