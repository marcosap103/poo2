# POO2

Programação Orientada a Objetos.
+ Repositório de arquivos de aula.

## Descrição
Repositório de Arquivos para a matéria de programação orientada a objetos do curso de Ciência da Computação do Instituto Federal Catarinense do qual faço parte. Trata-se de exercícios propóstos em sala sobre temas que abordam:
- Reflexão Computacional (Java API reflect).
- Reusabilidade.
- Criação de bibliotecas.
- Persistência (jdbc, hibernate).
- Persistência (sqlalchemy).
- Mapeamento objeto-relacional em dados existentes.
- Banco de dados orientado a objetos.
