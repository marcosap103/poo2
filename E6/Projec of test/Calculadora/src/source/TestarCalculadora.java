package source;

public class TestarCalculadora {
    public static void main(String[] args) {
        Calculadora calc = new Calculadora();
        
        float a = 10.0f;
        float b = 5.0f;
        
        System.out.println("Soma: " + calc.somar(a, b));
        System.out.println("Subtração: " + calc.subtrair(a, b));
        System.out.println("Multiplicação: " + calc.multiplicar(a, b));
        System.out.println("Divisão: " + calc.dividir(a, b));
    }
}
