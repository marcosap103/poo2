package source;

public class Calculadora {
	public Float subtrair(Float valorMaior, Float valorMenor) {
		return valorMaior - valorMenor;
	}
	
	public Float somar(Float valorInicial, Float valorFinal) {
		return valorInicial + valorFinal;
	}
	
	public Float multiplicar(Float valorInicial, Float valorFinal) {
		return valorInicial * valorFinal;
	}
	
	public Float dividir(Float valorInicial, Float valorFinal) {
		return valorInicial / valorFinal;
	}
}
	