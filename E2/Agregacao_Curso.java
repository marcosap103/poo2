package tipos_classes;
import tipos_classes.Estudante;
import java.util.ArrayList;
import java.util.List;

public class Agregacao_Curso {
    private final String nome;
    private final String professor;
    private final List<Estudante> estudantes;

    public Agregacao_Curso(String nome, String professor) {
        this.nome = nome;
        this.professor = professor;
        this.estudantes = new ArrayList<>();
    }

    public void addEstudante(Estudante estudante) {
        estudantes.add(estudante);
    }
}
