package tipos_classes;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class  Exercicio_E2{
    public static void main(String[] args) {
        // Obtém a classe String utilizando o método getClass()
        Class<?> stringClass = "".getClass();
        System.out.println("Nome da classe: " + stringClass.getName());

        // Obtém a classe Integer utilizando o método forName()
        try {
            Class<?> integerClass = Class.forName("java.lang.Integer");
            System.out.println("Nome da classe: " + integerClass.getName());
        } catch (ClassNotFoundException e) {
            System.out.println("Classe não encontrada: " + e.getMessage());
        }

        // Obtém as informações dos métodos da classe Integer
        Method[] methods = Integer.class.getMethods();
        for (Method method : methods) {
            System.out.println("Nome do método: " + method.getName());
            System.out.println("Tipo de retorno: " + method.getReturnType().getName());
            Class<?>[] parameterTypes = method.getParameterTypes();
            for (Class<?> parameterType : parameterTypes) {
                System.out.println("Tipo de parâmetro: " + parameterType.getName());
            }
            System.out.println();
        }

        // Obtém as informações dos campos da classe Integer
        Field[] fields = Integer.class.getFields();
        for (Field field : fields) {
            System.out.println("Nome do campo: " + field.getName());
            System.out.println("Tipo do campo: " + field.getType().getName());
            System.out.println();
        }
    }
}
