package tipos_classes;

public class Animal_Heranca {
    private final String Nome;

    public Animal_Heranca(String Nome) {
        this.Nome = Nome;
    }

    public void comer() {
        System.out.println(Nome + " esta comendo.");
    }
}
