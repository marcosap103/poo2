package tipos_classes;

public class Gato_Herdado extends Animal_Heranca { 
    //Gato herda metodos e atributos de animal
    public Gato_Herdado(String Nome) {
        super(Nome);//Atributo herdado da super classe
    }

    public void miau() {
        System.out.println("Miau!");
    }
}
