package tipos_classes;

public class Carro{
    private final String marca;
    private final String modelo;

    public Carro(String marca, String modelo, int ano) {
        this.marca = marca;
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void dirigir() {
        System.out.println("Dirigindo o " + marca + " " + modelo);
    }

    public void parar() {
        System.out.println("Parando o " + marca + " " + modelo);
    }
}
