from Persistence import Persistent

class Companiaz(Persistent):

    def __init__(self):
        self.n = "n"
        self.nome= "name"
        self.domain = "domain"
        self.year_founded= "year founded"
        self.industry = "industry"
        self.size_range = "size range"
        self.locality = "locality"
        self.country = "country"
        self.linkedin_url= "linkedin url"
        self.current_employee_estimate = "current employee estimate"
        self.total_employee_estimate = "total employee estimate"

    def __str__(self):
        return f'{self.n}, {self.nome}, {self.domain}, {self.year_founded}, {self.industry}, {self.size_range}, {self.locality}, {self.country}, {self.linkedin_url}, {self.current_employee_estimate}, {self.total_employee_estimate}'


